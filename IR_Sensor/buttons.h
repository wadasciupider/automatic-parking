
#ifndef buttons_h
#define buttons_h
#define SW1_PIN 3
#define SW3_PIN 12	
#include "MKL46Z4.h"                    // Device header

void buttonsInitialize(uint32_t);
int32_t Read_SW1(void);
int32_t Read_SW3(void);

#endif

