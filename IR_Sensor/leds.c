
#include "MKL46Z4.h"                    //Device header
#include "leds.h"												//Declarations

const uint32_t green_mask= 1UL<<5;				//Red led is Port D bit 5
const uint32_t red_mask= 1UL<<29;			//Green led is Port C bit 5/




/*----------------------------------------------------------------------------
  Function that initializes LEDs
 *----------------------------------------------------------------------------*/
void ledsInitialize(void) {
volatile int delay;
	
//Initialize registers	
  SIM->SCGC5 |=  (SIM_SCGC5_PORTD_MASK | SIM_SCGC5_PORTE_MASK);      /* Enable Clock to Port D & E */ 
  PORTD->PCR[5] = PORT_PCR_MUX(1);                       /* Pin PTD5 is GPIO */
  PORTE->PCR[29] = PORT_PCR_MUX(1);                      /* Pin PTE29 is GPIO */
  
	FPTE->PSOR = red_mask	;          /* switch Red LED off */
	FPTD->PSOR = green_mask	;        /* switch Green LED off */
  FPTE->PDDR = red_mask	;          /* enable PTB18/19 as Output */
	FPTD->PDDR = green_mask	;        /* enable PTB18/19 as Output */
	

//Welcome sequence
	for(delay=0; delay<1200000; delay++);
	FPTE->PCOR = red_mask	;          /* switch Red LED on */
	FPTD->PCOR = green_mask	;        /* switch Green LED on */
	for(delay=0; delay<1200000; delay++);
	FPTE->PSOR = red_mask	;          /* switch Red LED off */
	FPTD->PSOR = green_mask	;        /* switch Green LED off */
}



void Red_On (void) {
	FPTE->PCOR=red_mask;          	/* switch Red LED on */
}


void Green_On (void) {
	FPTD->PCOR=green_mask;       		/* switch Green LED on */
}

void Red_Off (void) {
	FPTE->PSOR=red_mask;          	/* switch Red LED on */
}


void Green_Off (void) {
	FPTD->PSOR=green_mask;       		/* switch Green LED on */
}
void Red_Toggle (void) {
	FPTE->PTOR=red_mask;          	/* switch Red LED on */
}


void Green_Toggle (void) {
	FPTD->PTOR=green_mask;       		/* switch Green LED on */
}

void ledsOff (void) {
		FPTE->PSOR=red_mask;          /* switch Red LED off  */
	  FPTD->PSOR=green_mask;        /* switch Green LED off  */
}


void ledsOn (void) {
		FPTE->PCOR=red_mask;      	/* switch Red LED on  */
	  FPTD->PCOR=green_mask;     	/* switch Green LED on */
}





