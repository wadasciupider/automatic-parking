#include "MKL46Z4.h" 
#include "lcd.h"
#define LCD_N_FRONT 8
#define LCD_N_BACK 4
#define LCD_S_D 0x11 // segment D
#define LCD_S_E 0x22 // segment E
#define LCD_S_G 0x44 // segment G
#define LCD_S_F 0x88 // segment F
#define LCD_S_DEC 0x11
#define LCD_S_C 0x22
#define LCD_S_B 0x44
#define LCD_S_A 0x88
#define LCD_C 0x00
#define MaskLED(x) (1UL << x);
const static uint8_t LCD_Front_Pin[LCD_N_FRONT] = {37u, 17u, 7u, 8u, 53u, 38u, 10u, 11u};
//const static uint8_t LCD_Back_Pin[LCD_N_BACK] = {40u, 52u, 19u,18u};

 void lcdInitialize(void){
SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK | SIM_SCGC5_PORTC_MASK | SIM_SCGC5_PORTD_MASK | SIM_SCGC5_PORTE_MASK | SIM_SCGC5_SLCD_MASK; // Dostarczenie zegara do wszystkich port�w
// wstepne wylaczenie i reset kontrolera
LCD->GCR |= LCD_GCR_PADSAFE_MASK; 
LCD->GCR &= ~LCD_GCR_LCDEN_MASK; // Clear LCDEN w trakcie konfiguracji
// konfiguracja multiplekser�w do operacji port�w jako kontroler LCD
	 PORTB->PCR[7] = PORT_PCR_MUX(0u);
	 PORTB->PCR[8] = PORT_PCR_MUX(0u);
	 PORTB->PCR[10] = PORT_PCR_MUX(0u);
	 PORTB->PCR[11] = PORT_PCR_MUX(0u);
	 PORTB->PCR[21] = PORT_PCR_MUX(0u);
	 PORTB->PCR[22] = PORT_PCR_MUX(0u);
	 PORTB->PCR[23] = PORT_PCR_MUX(0u);

	 PORTC->PCR[17] = PORT_PCR_MUX(0u);
	 PORTC->PCR[18] = PORT_PCR_MUX(0u);
	 
	 PORTD->PCR[0] = PORT_PCR_MUX(0u);
	 
	 PORTE->PCR[4] = PORT_PCR_MUX(0u);
	 PORTE->PCR[5] = PORT_PCR_MUX(0u);
// konfiguracja rejestr�w LCD
LCD->GCR = 
LCD_GCR_RVTRIM(0x00) |
LCD_GCR_CPSEL_MASK |
LCD_GCR_LADJ(0x03) |
LCD_GCR_VSUPPLY_MASK |
LCD_GCR_ALTDIV(0x00) |
LCD_GCR_SOURCE_MASK |
LCD_GCR_LCLK(0x01) |
LCD_GCR_DUTY(0x03);
// konfiguracja migania wyswietlacza
LCD->AR = LCD_AR_BRATE(0x03); 
// konfiguracja rejestru FDCR
LCD->FDCR = 0x00000000;
// aktywowanie 12 pin�w do kontroli wyswietlaczem (dwa rejestry po 32 bity)
LCD->PEN[0] =
LCD_PEN_PEN(1u<<7u ) | // LCD_P7
LCD_PEN_PEN(1u<<10u ) |
LCD_PEN_PEN(1u<<11u) |
LCD_PEN_PEN(1u<<8u ) |
LCD_PEN_PEN(1u<<17u ) |
LCD_PEN_PEN(1u<<18u ) |
LCD_PEN_PEN(1u<<19u ) ;
LCD->PEN[1] =
LCD_PEN_PEN(1u<<6u ) |
LCD_PEN_PEN(1u<<8u ) |
LCD_PEN_PEN(1u<<5u ) |
LCD_PEN_PEN(1u<<21u ) |
LCD_PEN_PEN(1u<<20u ) ;
// skonfigurowanie 4 pin�w plaszczyzny tylnej (dwa rejestry po 32 bity)
LCD->BPEN[0] =
LCD_BPEN_BPEN(1u<<18u) |
LCD_BPEN_BPEN(1u<<19u);
LCD->BPEN[1] =
LCD_BPEN_BPEN(1u<<8u) |
LCD_BPEN_BPEN(1u<<20u);
// konfiguracja rejestr�w przebieg�w (Waveform register) � 4 aktywne, reszta nie
// konfiguracja polega na r�wnomiernym rozlozeniu faz, w tym przypadku 4, na 8 bita
// (44.3.7 w KL46 Reference Manual)
LCD->WF[0] =
LCD_WF_WF0(0x00) |
LCD_WF_WF1(0x00) |
LCD_WF_WF2(0x00) |
LCD_WF_WF3(0x00);
LCD->WF[1] =
LCD_WF_WF4(0x00) |
LCD_WF_WF5(0x00) |
LCD_WF_WF6(0x00) |
LCD_WF_WF7(0x00);
LCD->WF[2] =
LCD_WF_WF8(0x00) |
LCD_WF_WF9(0x00) |
LCD_WF_WF10(0x00) |
LCD_WF_WF11(0x00);
LCD->WF[3] =
LCD_WF_WF12(0x00) |
LCD_WF_WF13(0x00) |
LCD_WF_WF14(0x00) |
LCD_WF_WF15(0x00);
LCD->WF[4] =
LCD_WF_WF16(0x00) |
LCD_WF_WF17(0x00) |
LCD_WF_WF18(0x88) | // COM3 (10001000)
LCD_WF_WF19(0x44); // COM2 (01000100)
LCD->WF[5] =
LCD_WF_WF20(0x00) |
LCD_WF_WF21(0x00) |
LCD_WF_WF22(0x00) |
LCD_WF_WF23(0x00);
LCD->WF[6] =
LCD_WF_WF24(0x00) |
LCD_WF_WF25(0x00) |
LCD_WF_WF26(0x00) |
LCD_WF_WF27(0x00);
LCD->WF[7] =
LCD_WF_WF28(0x00) |
LCD_WF_WF29(0x00) |
LCD_WF_WF30(0x00) |
LCD_WF_WF31(0x00);
LCD->WF[8] =
LCD_WF_WF32(0x00) |
LCD_WF_WF33(0x00) |
LCD_WF_WF34(0x00) |
LCD_WF_WF35(0x00);
LCD->WF[9] =
LCD_WF_WF36(0x00) |
LCD_WF_WF37(0x00) |
LCD_WF_WF38(0x00) |
LCD_WF_WF39(0x00);
LCD->WF[10] =
LCD_WF_WF40(0x11) |  // COM0 (00010001)
LCD_WF_WF41(0x00) |
LCD_WF_WF42(0x00) |
LCD_WF_WF43(0x00);
LCD->WF[11] =
LCD_WF_WF44(0x00) |
LCD_WF_WF45(0x00) |
LCD_WF_WF46(0x00) |
LCD_WF_WF47(0x00);
LCD->WF[12] =
LCD_WF_WF48(0x00) |
LCD_WF_WF49(0x00) |
LCD_WF_WF50(0x00) |
LCD_WF_WF51(0x00);
LCD->WF[13] =
LCD_WF_WF52(0x22) |  // COM1 (00100010)
LCD_WF_WF53(0x00) |
LCD_WF_WF54(0x00) |
LCD_WF_WF55(0x00);
LCD->WF[14] =
LCD_WF_WF56(0x00) |
LCD_WF_WF57(0x00) |
LCD_WF_WF58(0x00) |
LCD_WF_WF59(0x00);
LCD->WF[15] =
LCD_WF_WF60(0x00) |
LCD_WF_WF61(0x00) |
LCD_WF_WF62(0x00) |
LCD_WF_WF63(0x00);
// koniec konfiguracji, wiec clear PADSAFE i wlaczenie wyswietlacza
LCD->GCR &= ~LCD_GCR_PADSAFE_MASK; // !!!!!!
LCD->GCR |= LCD_GCR_LCDEN_MASK; // wlaczenie wyswietlacza
}

void lcd_set(uint8_t value,uint8_t digit){
// value � wyswietlana wartosc,
// digit � pozycja na kt�rej ma byc wyswietlona wartosc
if(value==0x00){  // aby wyswietlic �0� zapalamy segmenty
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] =  // D, E, F oraz A, B, C
(LCD_S_D | LCD_S_E |LCD_S_F);  // (patrz tabelki w zadaniu 2.1)
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] =
(LCD_S_A | LCD_S_B | LCD_S_C);
}
else if(value==0x01){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] =
(LCD_C);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] =
(LCD_S_B | LCD_S_C);
}
else if(value==0x02){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_E | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_B);
}
else if(value==0x03){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_B | LCD_S_C);
}
else if(value==0x04){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_B | LCD_S_C);
}
else if(value==0x05){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_C);
}
else if(value==0x06){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_E | LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_C);
}
else if(value==0x07){
	LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_C);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_B | LCD_S_C);
}
else if(value==0x08){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_E | LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_B | LCD_S_C);
}
else if(value==0x09){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_B | LCD_S_C);
}
else if(value==0x0A){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_E | LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_B | LCD_S_C);
}
else if(value==0x0B){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_E | LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_C);
}
else if(value==0x0C){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_E | LCD_S_F);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A);
}
else if(value==0x0D){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_E | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_B | LCD_S_C);
}
else if(value==0x0E){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_D | LCD_S_E | LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A);
}
else if(value==0x0F){
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_E | LCD_S_F | LCD_S_G);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A);
}
else if(value==16) {
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_E | LCD_S_F | LCD_S_D);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_B | LCD_S_C );
}
else if(value==17) {
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] = (LCD_S_F | LCD_S_G | LCD_S_E);
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] = (LCD_S_A | LCD_S_B );
}
else if(value==18) {
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] |= LCD_C;
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] |= LCD_S_DEC;
}
else if(value==19) {
LCD->WF8B[LCD_Front_Pin[((2*digit)-2)]] |= LCD_C;
LCD->WF8B[LCD_Front_Pin[((2*digit)-1)]] &= ~LCD_S_DEC;
}
}
void lcd_dec(uint32_t liczba){
lcd_set(liczba%10,4);
lcd_set((liczba%100-liczba%10)/10,3);
lcd_set((liczba%1000-liczba%100)/100,2);
lcd_set((liczba-liczba%1000)/1000,1);
}
void lcd_hex(uint32_t liczba){
lcd_set(liczba%16,4);
lcd_set((liczba%256-liczba%16)/16,3);
lcd_set((liczba%4096-liczba%256)/256,2);
lcd_set((liczba-liczba%4096)/4096,1);
}
void lcd_dot(uint8_t digit)
{
	lcd_set(18,digit); //1 2 3 dots, 4- dwukropek
}
