#ifndef leds_h
#define leds_h

void ledsInitialize(void);

void ledsOff (void);
void ledsOn (void);
void Green_Off (void);
void Red_Off (void);
void Green_On (void);
void Red_On (void);
void Green_Toggle (void);
void Red_Toggle (void);

#endif
