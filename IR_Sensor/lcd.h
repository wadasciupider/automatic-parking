#ifndef lcd_h
#define lcd_h
#include "MKL46Z4.h" 
void lcdInitialize(void);
void lcd_set(uint8_t value,uint8_t digit);
void lcd_dec(uint32_t);
void lcd_hex(uint32_t);
void lcd_dot(uint8_t digit);
#endif
