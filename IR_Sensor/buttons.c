
#include "MKL46Z4.h"                    /* Device header */
#include "buttons.h"
#include "leds.h"
#define SW1_PIN 3												/* SW1 switch is on port C - pin 3 */
#define SW3_PIN 12											/* SW3 switch is on port C - pin 12 */

#define PORTC_D_IRQ_NBR 31 				



/*----------------------------------------------------------------------------
	Function initializes port C pin for SW1 SW2 handling
	and optionally enables PORT_C_D interrupts
 *----------------------------------------------------------------------------*/
void buttonsInitialize(uint32_t interrupts){
	
	SIM->SCGC5 |=  SIM_SCGC5_PORTC_MASK; 					/* Enable clock for port C */
	PORTC->PCR[SW1_PIN] |= PORT_PCR_MUX(1);      	/* Pin PTC3 is GPIO */
	PORTC->PCR[SW3_PIN] |= PORT_PCR_MUX(1);
	
	/* Port control register for bit 3,12 of port C configuration. Activate pull up and interrupt */
	
	PORTC->PCR[SW1_PIN] |=  PORT_PCR_PE_MASK |		
													PORT_PCR_PS_MASK;		
	PORTC->PCR[SW1_PIN] |= 	PORT_PCR_IRQC(10);	
	
	PORTC->PCR[SW3_PIN] |=  PORT_PCR_PE_MASK |		
													PORT_PCR_PS_MASK;		
	PORTC->PCR[SW3_PIN] |= 	PORT_PCR_IRQC(10);	
	
	/*ARM's Nested Vector Interrupt Controller configuration*/
	if(interrupts==1){
	NVIC_ClearPendingIRQ(PORTC_D_IRQ_NBR);				
	NVIC_EnableIRQ(PORTC_D_IRQ_NBR);
	NVIC_SetPriority (PORTC_D_IRQ_NBR, 1);		
	}
	/*Priority*/
	//NVIC_SetPriority (PORTC_D_IRQ_NBR, 3);			

}

/*----------------------------------------------------------------------------
	Function reads SW 1, SW 3 state
*----------------------------------------------------------------------------*/
int32_t Read_SW1(){
	return FPTC->PDIR & (1UL<<SW1_PIN);						
}	
int32_t Read_SW3(){
	return FPTC->PDIR & (1UL<<SW3_PIN);						
}

