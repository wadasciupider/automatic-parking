#include "MKL46Z4.h"                    	
#include "IR.h"
#include "buttons.h"
#include "motorDriver2.h"

uint8_t Speed = 20;
uint8_t Speed_Left;
uint8_t Speed_Right;
int i, Flag_space1=0,Flag_space2=0, Flag_parked=0, Flag=1;

//	Funkcja parkowania rownoleglego	//
void Parallel(void){
	int i;
	driveReverseDistLeftTrack(50,53);
	driveReverseDistRightTrack(15,30);
		for(i=0;i<11500000;i++){
		}
	driveReverseDist(40,7);
		for(i=0;i<1000000;i++){
		}
	driveReverseDistRightTrack(45,32);
}

//	Funkcja wyjazdu rownoleglego	//
void Parallel_Out(void){
	int i;
	driveForwardDistRightTrack(45,35);
		for(i=0;i<11500000;i++){
			}
	driveForwardDist(20,3);
		for(i=0;i<1000000;i++){
		}
	driveForwardDistLeftTrack(50,57);
	driveForwardDistRightTrack(15,20);
}

//	Funkcja parkowania prostopadlego	//
void Perpendicular(void){
	int i;
	driveReverseDistLeftTrack(60,58);
	driveReverseDistRightTrack(15,25);
		for(i=0;i<12000000;i++){
		}
	driveReverseDist(40,15);
}

//	Funkcja wyjazdu prostopadlego	//
void Perpendicular_Out(void){
	int i;
	driveForwardDist(20,2);
		for(i=0;i<5000000;i++){
		}
	driveForwardDistLeftTrack(50,58);
	driveForwardDistRightTrack(15,20);
	Flag_space2=0;
}

//	Obsluga przerwania przyciskow do wyjazdu	//
void PORTC_PORTD_IRQHandler(void){
		for(i=0;i<5000000;i++){
		}
	if((Flag_space2==1)&&(Flag_parked==1)){
		Perpendicular_Out();
			for(i=0;i<4000000;i++){
			}
		Flag_parked=0;
	}
	else if((Flag_space2==0)&&(Flag_parked==1)){
		Parallel_Out();
			for(i=0;i<8000000;i++){
			}
		Flag_parked=0;
	}
	PORTC->PCR[SW1_PIN] |= PORT_PCR_ISF_MASK; 
	PORTC->PCR[SW3_PIN] |= PORT_PCR_ISF_MASK;	
}
			
void SysTick_Handler(void) {
	int i;
	if(Flag==1){											// Dojazd prostopadly do rzedu samochodow 
		driveForward(50);
			if((Get_distance(2)/10)<190){
				driveStop();
				Flag=0;
					for(i=0;i<1000000;i++){
					}
				driveForwardDistRightTrack(30,30);
				while(Get_flag()==0){
				}
					for(i=0;i<1000000;i++){
					}
			}
		}
	if((Flag_parked==0)&&(Flag==0)){	// Jazda rownolegla
		if((Get_distance(0)/10)>((Get_distance(1)/10))){
			if(Speed_Right>Speed)
				Speed_Right--;
			else if(Speed_Left<100)
				Speed_Left++;
		}
		else if(((Get_distance(0)/10))<((Get_distance(1)/10))){
			if(Speed_Left>Speed)
				Speed_Left--;
			else if(Speed_Right<100)
				Speed_Right++;
		}
		driveForwardLeftTrack(Speed_Left);
		driveForwardRightTrack(Speed_Right);
		if ((Get_distance(0)/10)>200){	// Wykrycie wolnego miejsca
			driveForwardDist(30,18);
			while(Get_flag()==0){
				if((Get_distance(0)/10)<200){
					Flag_space1=1;
				}
			}
			if(Flag_space1==0){						// Sprawdzanie ilosci dostepnego wolnego miejsca
				driveForwardDist(30,18);
				while(Get_flag()==0){
					if((Get_distance(0)/10)<200){
						Flag_space2=1;
					}
				}
			if(Flag_space2==0){						// Miejsce na parkowanie rownolegle
				driveForwardDist(30,22);
				while(Get_flag()==0){
				}
					for(i=0;i<1000000;i++){
					}
				Parallel();
				Flag_parked=1;
			}
			else{													// Miejsce na parkowanie prostopadle 
				driveForwardDist(30,8);
				while(Get_flag()==0){
				}
					for(i=0;i<1000000;i++){
					}
				Perpendicular();
				Flag_parked=1;
			}
			}
			Flag_space1=0;
		}				
	}
}	

int main (void) {
	Speed_Left = Speed;
	Speed_Right = Speed;
	motorDriverInit();
	IRInitialize(7);
	buttonsInitialize(1);
	SystemCoreClockUpdate();	
	SysTick_Config(SystemCoreClock/10);
	while(1){ 
	}
}
